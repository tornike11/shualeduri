package com.example.shualeduri

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var regButton: Button
    private lateinit var editName: EditText
    private lateinit var editSurname: EditText
    private lateinit var editTextPhone: EditText
    private lateinit var editPir: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        regButton = findViewById(R.id.regButton)
        editTextPhone = findViewById(R.id.editTextPhone)
        editPir = findViewById(R.id.editPir)
        editName = findViewById(R.id.editName)
        editSurname = findViewById(R.id.editSurname)

        regButton.setOnClickListener {
            if(editName.text.isEmpty()){
                Toast.makeText(this, "სახელის ველი ცარიელია", Toast.LENGTH_LONG).show()
            }
            else if(editSurname.text.isEmpty()){
                Toast.makeText(this, "გვარის ველი ცარიელია", Toast.LENGTH_LONG).show()
            }
            else if(editTextPhone.text.isEmpty()){
                Toast.makeText(this, "ტელეფონის ველი ცარიელია", Toast.LENGTH_LONG).show()
            }
            else if(editPir.text.isEmpty()){
                Toast.makeText(this, "პირადი ნომრის ველი ცარიელია", Toast.LENGTH_LONG).show()
            }
            else if(editName.text.length < 3){
                Toast.makeText(this, "სახელი უნდა შეიცავდეს მინიმუმ 3 სიმბოლოს", Toast.LENGTH_LONG).show()
            }
            else if(editSurname.text.length < 5){
                Toast.makeText(this, "გვარი უნდა შეიცავდეს მინიმუმ 5 სიმბოლოს", Toast.LENGTH_LONG).show()
            }
            else if(editTextPhone.text.length < 9){
                Toast.makeText(this, "ტელეფონის ნომერი უნდა შეიცავდეს მინიმუმ 9 სიმბოლოს", Toast.LENGTH_LONG).show()
            }
            else if(editPir.text.length  !== 11){
                Toast.makeText(this, "პირადობის ნომერი არ არის თერთმეტნიშნა", Toast.LENGTH_LONG).show()
            }
        }


    }
}